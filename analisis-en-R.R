
# Cargar librerías --------------------------------------------------------

library(tidyverse)

# Cargar datos  -----------------------------------------------------------

df <- read_delim("../script_aplausos experiment-table.csv", delim = ",", skip = 6)    

# Dar formato a los datos -------------------------------------------------

colnames(df) <- c("corrida", "porcentaje", "presion", "ciclos", "paso", "aplausos")

# Formato a columnas
df$corrida <- factor(df$corrida)

# Subset
df <- subset(df, paso <= 35) 

dfs <- subset(df, corrida == 1 | corrida == 10 | corrida == 100 | corrida == 1000)

# Plots -------------------------------------------------------------------

g <- ggplot(df)

# BoxPlots ----------------------------------------------------------------

# Para todas las combinaciones
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", subtitle = "Todas las combinaciones de parámetros")

# Agrupado por ciclos
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(ciclos))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", subtitle = "Agrupado por parámetro: ciclo")


# Agrupado por porcentajes
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(porcentaje))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", subtitle = "Agrupado por parámetro: porcentajes")

# Agrupado por presion
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(presion))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", subtitle = "Agrupado por parámetro: presion")


# Scatter plots -----------------------------------------------------------

g + 
  geom_point(aes(x = paso, y = aplausos, group = corrida, color = corrida)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Scatterplot de aplausos", subtitle = "Corridas 1, 10, 100 y 1000")

g + 
  geom_area(aes(x = paso, y = aplausos, group = corrida, color = corrida)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Scatterplot de aplausos", subtitle = "Corridas 1, 10, 100 y 1000")


g + 
  geom_bin2d(aes(x = paso, y = aplausos), binwidth = c(1, 50))



# Agrupar -----------------------------------------------------------------

# Pasos y ciclos
df.by.paso_ciclos <- df %>% 
  group_by(paso, ciclos)

prom_aplausos.by.paso_ciclos <- df.by.paso_ciclos %>% summarise(prom_aplausos = mean(aplausos))

ggplot(prom_aplausos.by.paso_ciclos) +
  geom_line(aes(x = paso, y = prom_aplausos, group = ciclos, color = ciclos)) + 
  geom_point(aes(x = paso, y = prom_aplausos, group = ciclos, color = ciclos)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Scatterplot del promedio de aplausos", subtitle = "Agrupado por parámetro: ciclos")

# Pasos y porcentajes
df.by.paso_porcentaje <- df %>% 
  group_by(paso, porcentaje)

prom_aplausos.by.paso_porcentaje <- df.by.paso_porcentaje %>% summarise(prom_aplausos = mean(aplausos))

ggplot(prom_aplausos.by.paso_porcentaje) +
  geom_line(aes(x = paso, y = prom_aplausos, group = porcentaje, color = porcentaje)) + 
  geom_point(aes(x = paso, y = prom_aplausos, group = porcentaje, color = porcentaje)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Scatterplot del promedio de aplausos", subtitle = "Agrupado por parámetro: porcentaje")
