---
title: "Introducción a los Sistemas Complejos"
author: "Andrés de la Rosa, Federico Rodríguez, Guzmán López, Luis Claro, Mariana Illarze"
date: "November 8, 2017"
output: 
  pdf_document: 
    fig_caption: yes
    highlight: pygments
    keep_tex: yes
    latex_engine: xelatex
    number_sections: yes
    toc: yes
---

# Introducción

# Metodología

## NetLogo

## Análisis en R

```{r}

# Cargar librerías
library(tidyverse)

```

```{r}

# Cargar datos
df <- read_delim("../script_aplausos experiment-table.csv", 
                 delim = ",", skip = 6) 

```

```{r}

# Dar formato a los datos -------------------------------------------------

colnames(df) <- c("corrida", "porcentaje", "presion", 
                  "ciclos", "paso", "aplausos")

# Formato a columnas
df$corrida <- factor(df$corrida)

# Subset
df <- subset(df, paso <= 35) 

```

# Resultados

```{r}

# Objeto ggplot2 
g <- ggplot(df)

```


```{r}

# BoxPlots

# Para todas las combinaciones
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Todas las combinaciones de parámetros")

# Agrupado por ciclos
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(ciclos))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Agrupado por parámetro: ciclo")

# Agrupado por porcentajes
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(porcentaje))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Agrupado por parámetro: porcentajes")

# Agrupado por presion
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(presion))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Agrupado por parámetro: presion")


```

```{r}

g + 
  geom_bin2d(aes(x = paso, y = aplausos), binwidth = c(1, 50))

```

```{r}

# Agrupar

# Pasos y ciclos
df.by.paso_ciclos <- df %>% 
  group_by(paso, ciclos)

prom_aplausos.by.paso_ciclos <- df.by.paso_ciclos %>% 
  summarise(prom_aplausos = mean(aplausos))

ggplot(prom_aplausos.by.paso_ciclos) +
  geom_line(aes(x = paso, y = prom_aplausos, group = ciclos, color = ciclos)) + 
  geom_point(aes(x = paso, y = prom_aplausos, group = ciclos, color = ciclos)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", 
       title = "Scatterplot del promedio de aplausos", 
       subtitle = "Agrupado por parámetro: ciclos")

# Pasos y porcentajes
df.by.paso_porcentaje <- df %>% 
  group_by(paso, porcentaje)

prom_aplausos.by.paso_porcentaje <- df.by.paso_porcentaje %>% 
  summarise(prom_aplausos = mean(aplausos))

ggplot(prom_aplausos.by.paso_porcentaje) +
  geom_line(aes(x = paso, y = prom_aplausos, 
                group = porcentaje, color = porcentaje)) + 
  geom_point(aes(x = paso, y = prom_aplausos, 
                 group = porcentaje, color = porcentaje)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", 
       title = "Scatterplot del promedio de aplausos", 
       subtitle = "Agrupado por parámetro: porcentaje")


```

# Agradecimientos

Beca, Cabañas, comidas, oportunidad, etc